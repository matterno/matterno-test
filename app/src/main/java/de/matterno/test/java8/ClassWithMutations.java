package de.matterno.test.java8;

public class ClassWithMutations {

    public static final String withMutations(final String string) {
        String.join(string, "lol");
        return string;
    }

}
