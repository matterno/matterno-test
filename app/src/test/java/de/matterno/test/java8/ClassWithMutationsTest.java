package de.matterno.test.java8;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;


@RunWith(MockitoJUnitRunner.class)
public class ClassWithMutationsTest {

    @InjectMocks
    private ClassWithMutations underTest;

    @Test
    public void testClassWithMutationsTest() {
        assertEquals("test", ClassWithMutations.withMutations("test"));
    }
}
